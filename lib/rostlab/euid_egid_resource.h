/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_EUID_EGID
#define ROSTLAB_EUID_EGID 1

#include <errno.h>
#include <sstream>
#include <stdexcept>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "rostlab/rostlab_stdexcept.h"

namespace rostlab {

class euid_egid_resource
{
  private:
    uid_t         _olduid;
    gid_t         _oldgid;
    bool          _changeduid;
    bool          _changedgid;
    // this is a resource - disable copy contructor and copy assignment
                  euid_egid_resource( const euid_egid_resource& ){};
    euid_egid_resource&
                  operator=(const euid_egid_resource&){return *this;};

  public:
                  euid_egid_resource( uid_t __neweuid, gid_t __newegid ) : _changeduid(false), _changedgid(false)
    {
      _olduid = getuid();
      _oldgid = getgid();
      if( getegid() != __newegid )
      {
        if( setregid( getegid(), __newegid ) ){ std::ostringstream s; s << "failed to setregid " << getegid() << ":" << __newegid << " : " << strerror( errno ); throw runtime_error( s.str() ); }
        _changedgid = true;
      }
      if( geteuid() != __neweuid )
      {
        if( setreuid( geteuid(), __neweuid ) ){ std::ostringstream s; s << "failed to setreuid " << geteuid() << ":" << __neweuid << " : " << strerror( errno ); throw runtime_error( s.str() ); }
        _changeduid = true;
      }
    }

    virtual       ~euid_egid_resource()
    {
      if( _changeduid )
      {
        if( setreuid( geteuid(), getuid() ) ){ std::ostringstream s; s << "failed revert setreuid to " << geteuid() << ":" << getuid() << " : " << strerror( errno ); throw runtime_error( s.str() ); }
        if( setreuid( _olduid, -1 ) ){ std::ostringstream s; s << "failed revert setreuid to " << _olduid << ":-1" << " : " << strerror( errno ); throw runtime_error( s.str() ); }
      }
      if( _changedgid )
      {
        if( setregid( getegid(), getgid() ) ){ std::ostringstream s; s << "failed revert setregid to " << getegid() << ":" << getgid() << " : " << strerror( errno ); throw runtime_error( s.str() ); }
        if( setregid( _oldgid, -1 ) ){ std::ostringstream s; s << "failed revert setregid to " << _oldgid << ":-1" << " : " << strerror( errno ); throw runtime_error( s.str() ); }
      }
    }
};

};

#endif // ROSTLAB_EUID_EGID
// vim:et:ai:ts=2:
