/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_FLOCK_RESOURCE
#define ROSTLAB_FLOCK_RESOURCE

#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "rostlab/rostlab_stdexcept.h"

namespace rostlab {

/** File locking resource implemented with fcntl()/POSIX locking API
 **
 ** fcntl()/POSIX locking also works with NFS.
*/
class file_lock_resource
{
  private:
    std::string        _filename;
    FILE*         _fh;
    // this is a resource - disable copy contructor and copy assignment
                  file_lock_resource( const file_lock_resource& ){};
    file_lock_resource&
                  operator=(const file_lock_resource&){return *this;};
  public:
    bool          dbg;

    class wouldblock : public exception {};

  public:
                  file_lock_resource() : _fh(NULL), dbg(false) {};

    /** __mode: "r", "r+", "w", "w+", "a", "a+"; __cmd: F_SETLK, F_SETLKW; __type: F_RDLCK, F_WRLCK
    */
                  file_lock_resource( const std::string& __file, const std::string& __mode = "r", int __cmd = F_SETLKW, short __type = F_RDLCK, bool __dbg = false ) : _fh(NULL), dbg(__dbg)
    {
      acquire( __file, __mode, __cmd, __type, __dbg );
    }

    inline void   acquire( const std::string& __file, const std::string& __mode = "r", int __cmd = F_SETLKW, short __type = F_RDLCK, bool __dbg = false ) throw ( runtime_error, wouldblock )
    {
      dbg = __dbg;
      release();
      //
      _filename = __file;
      _fh = fopen( _filename.c_str(), __mode.c_str() );
      if( !_fh ) throw runtime_error( strerror( errno ) );

      // instead of flock us fcntl()/POSIX locking so that it works with NFS as well
      flock lock;
      lock.l_type = __type;
      lock.l_whence = SEEK_SET;
      lock.l_start = 0;
      lock.l_len = 0;

      if( fcntl( fileno(_fh), __cmd, &lock ) )
      {
        int fcntl_errno = errno;
        fclose(_fh); _fh = NULL;
        if( fcntl_errno == EACCES || fcntl_errno == EAGAIN ) throw wouldblock();
        else throw runtime_error( strerror( fcntl_errno ) );
      }
      else if(dbg) std::cerr << "LOCK_" << __mode << " " << __cmd << " " << __type << " '" << _filename << "'\n";
    }

    inline void   release() throw (runtime_error)
    {
      if( _fh )
      {
        // lkajan: no need to explicitly release the lock: it is released when the file is closed
        if( fclose(_fh) ) throw runtime_error( strerror( errno ) );
        if(dbg) std::cerr << "LOCK_UN '" << _filename << "'\n";
        _fh = NULL;
      }
    }

    virtual       ~file_lock_resource()
    {
      release();
    }

    operator      FILE* () { return _fh; }

    inline const std::string&
                  filename() const { return _filename; }
};

};

#endif // ROSTLAB_FLOCK_RESOURCE
// vim:et:ts=2:ai:
