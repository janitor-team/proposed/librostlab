// Simple example file for autopkgtest: 
// loads blosum62 matrix from librostlab and prints it.
// Author: Tatiana Malygina <merlettaia@gmail.com>

#include <iostream>
#include <rostlab/blosum62.h>

const signed char rostlab::blosum62[27][27] = {};

int main() {
  for (int i = 0; i < 27; i++) {
    for (int j = 0; j < 27; j++) {
      std::cout << int(rostlab::blosum62[i][j]) << " ";
    }
    std::cout << std::endl;
  }
  return 0;
}

// Compile with:
// g++ -Wall -lrostlab blosum62_test.cpp -o blosum62_test 
