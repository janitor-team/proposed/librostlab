/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_CWD_RESOURCE
#define ROSTLAB_CWD_RESOURCE

#include <errno.h>
#include <stdexcept>
#include <unistd.h>
#include <string.h>

namespace rostlab {

class cwd_resource
{
  private:
    string        _olddir;
    // this is a resource - disable copy contructor and copy assignment
                  cwd_resource( const cwd_resource& ){};
    cwd_resource&
                  operator=(const cwd_resource&){return *this;};
  public:
                  cwd_resource() : _olddir("") {};

                  cwd_resource( const string& __dirname ) : _olddir("")
    {
      acquire( __dirname );
    }

    inline void   acquire( const string& __dirname ) throw (runtime_error)
    {
      release();
      //
      char *buf = NULL;
      if( ( buf = getcwd( NULL, 0 ) ) == NULL ) throw runtime_error( string("failed to get working directory: ") + strerror( errno ) );
      string cwd = buf; free( buf ); buf = NULL;

      if( chdir( __dirname.c_str() ) ) throw runtime_error( "failed to chdir to '" + __dirname + "': " + strerror( errno ) );
      _olddir = cwd;
    }

    inline void   release() throw (runtime_error)
    {
      if( _olddir != "" )
      {
        if( chdir( _olddir.c_str() ) ) throw runtime_error( "failed to chdir to '" + _olddir + "': " + strerror( errno ) );
        _olddir = "";
      }
    }

    virtual       ~cwd_resource()
    {
      release();
    }
};

};

#endif // ROSTLAB_CWD_RESOURCE
// vim:et:ts=2:ai:
