/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_AUX_FUNTIONS
#define ROSTLAB_AUX_FUNTIONS 1

#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace rostlab {

template<typename _Tp>
inline std::basic_string<_Tp>
                  join( const std::basic_string<_Tp>& __sep, typename std::vector< std::basic_string<_Tp> >::const_iterator __begin, typename std::vector< std::basic_string<_Tp> >::const_iterator __end )
{
  std::basic_string<_Tp> buf;
  for( typename std::vector< std::basic_string<_Tp> >::const_iterator v_i = __begin; v_i != __end; ++v_i )
  {
    if( buf.size() ) buf += __sep; buf += *v_i;
  }
  return buf;
}


template<typename _Tp>
inline std::basic_string<_Tp>
                  join( const std::basic_string<_Tp>& __sep, const std::vector< std::basic_string<_Tp> >& __v )
{
  return join( __sep, __v.begin(), __v.end() );
}


template<typename _Tp>
inline std::basic_string<_Tp>
                  join( const _Tp* __sep, const std::vector< std::basic_string<_Tp> >& __v )
{
  return join( std::string(__sep), __v );
}


template<typename _Tp>
inline std::basic_string<_Tp>
                  join( const _Tp __sep, const std::vector< std::basic_string<_Tp> >& __v )
{
  return join( std::string( 1, __sep ), __v );
}


inline std::vector<std::string>
                  split( const std::string& __str, char __c )
{
  std::vector<std::string>  ret;
  if( __str.empty() ) return ret;
  std::string::size_type pos = 0;
  do { std::string::size_type new_pos = __str.find( __c, pos ); ret.push_back( new_pos != std::string::npos ? __str.substr( pos, new_pos-pos ) : __str.substr( pos ) ); pos = new_pos; if( pos != std::string::npos ) ++pos; } while( pos != std::string::npos );
  return ret;
}


template<typename _Tk, typename _Tv>
inline std::vector<_Tk>
                  map_keys( const std::map<_Tk, _Tv>& __map )
{
  std::vector<_Tk>     ret;
  ret.reserve( __map.size() );
  for( typename std::map<_Tk, _Tv>::const_iterator m_i = __map.begin(); m_i != __map.end(); ++m_i ) ret.push_back( m_i->first );
  return ret;
}


inline void       split_in_2( const std::string& __str, char __c, std::string& __left, std::string& __right )
{
  std::string::size_type pos = __str.find( __c );
  if( pos == std::string::npos )
  {
    __left = __str;
  }
  else
  {
    __left = __str.substr( 0, pos );
    __right = __str.substr( pos+1 );
  }
}


inline std::map<std::string, std::string>
                  map_split_in_2( const std::vector<std::string>& __svec, char __c )
{
  std::map<std::string, std::string>       ret;
  for( std::vector<std::string>::const_iterator s_i = __svec.begin(); s_i != __svec.end(); ++s_i )
  {
    std::string left, right;
    split_in_2( *s_i, __c, left, right );
    ret[left] = right;
  }
  return ret;
}


//class ios_base {
//  private:
//    static int      _flagslot(){ static int slot = std::ios_base::xalloc(); return slot; }
//  public:
//    /// Stream formatting flags.
//    /** The flags are:
//     *  - tabdelim
//     */
//    typedef enum _Ios_Fmtflags {
//        /// Tab-delimited output format flag.
//        _S_tabdelim         = 1L << 1
//    } fmtflags;
//    /// Print in tab-delimited format.
//    /** This makes sense for example when printing maps. */
//    static const fmtflags tabdelim =    _S_tabdelim;
//
//    static fmtflags flags( std::ostream &os ) { return static_cast<fmtflags>( os.iword(_flagslot()) ); }
//    static fmtflags flags( std::ostream &os, const fmtflags f )
//    {
//        fmtflags ret = static_cast<fmtflags>( os.iword(_flagslot()) );
//        os.iword(_flagslot()) = f;
//        return ret;
//    }
//} ;
//
//
//template<typename K, typename V, class C, class A>
//inline
//std::ostream&     operator<< (std::ostream &os, const rostlab::ios_base::fmtflags f )
//{
//    rostlab::ios_base::flags(os, f);
//    return os;
//}


/// std::pair output operator.
template<class _T1, class _T2>
inline
std::ostream &    operator<< (std::ostream &os, const std::pair<_T1, _T2>& v)
{
  os << v.first << " => " << v.second;
  return os;
}


/// std::map output operator.
template<typename K, typename V, class C, class A>
inline
std::ostream&     operator<< (std::ostream &os, const std::map<K,V,C,A>& m)
{
    os << "{ ";
    for( typename std::map<K,V,C,A>::const_iterator p = m.begin(), p_end = m.end(); p != p_end; ++p )
    {
        if( p != m.begin() ) os << ", ";
        os << *p;
    }
    os << " }";
    return os;
}


/// std::vector output operator.
template<typename _Tp, typename _Alloc>
inline
std::ostream &    operator<< (std::ostream &os, const std::vector<_Tp, _Alloc>& v)
{
  typename std::vector<_Tp, _Alloc>::const_iterator v_i;
  os << "[ ";
  for( v_i = v.begin(); v_i != v.end(); ++v_i )
  { if( v_i != v.begin() ) os << ", "; os << *v_i; }
  os << " ]";
  return os;
}


}; // namespace rostlab

#endif // ROSTLAB_AUX_FUNTIONS
// vim:et:ts=4:ai:
