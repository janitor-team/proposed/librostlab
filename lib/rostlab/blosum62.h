/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BLOSUM62_H
#define BLOSUM62_H
namespace rostlab {
/// BLOSUM62 matrix at a scale of ln(2)/2.0.
/** '*' is column/row 0; other capital letters are in column ( letter & 0x1f ).  Columns/rows for non-aa letters such as o and u are 0.  The matrix is symmetrical.
 */
extern const signed char blosum62[27][27];
}
#endif // BLOSUM62_H
// vim:ts=4:et:ai:
