/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_STDIO
#define ROSTLAB_STDIO 1

#include <stdint.h>
#include <stdio.h>

namespace rostlab {

template<typename _Tp>
inline void         fwrite( const _Tp& __v, FILE* __out ){ if( ::fwrite( &__v, sizeof(__v), 1, __out ) != 1 ) throw rostlab::runtime_error( strerror(errno) ); }
template<typename _Tp, typename _Alloc>
inline void         fwrite( const vector<_Tp,_Alloc>& __v, FILE* __out )
{
    if( __v.size() > 0xffff ){ ostringstream s; s << __v.size() << " is out of range (0-65535)"; throw rostlab::range_error( s.str() ); }
    fwrite<uint16_t>( __v.size(), __out );
    if( ::fwrite( __v.data(), sizeof(typename vector<_Tp,_Alloc>::value_type), __v.size(), __out ) != __v.size() ) throw rostlab::runtime_error( strerror(errno) );
}
template<>
inline void         fwrite<string>( const string& __str, FILE* __out )
{
    fwrite<uint16_t>( __str.length(), __out );
    if( !__str.length() ) return;
    if( ::fwrite( __str.c_str(), sizeof( string::value_type ), __str.length(), __out ) != __str.length() ) throw rostlab::runtime_error( strerror(errno) );
}
inline void         fwrite( const char* __c, FILE* __out ) { fwrite( string(__c), __out ); }

template<typename _Tp>
inline void         fread( _Tp& __v, FILE* __in )
{
    if( ::fread( &__v, sizeof(__v), 1, __in ) != 1 ) throw rostlab::runtime_error( strerror(errno) );
}
template<typename _Tp, typename _Alloc>
inline void         fread( vector<_Tp, _Alloc>& __v, FILE* __in )
{
    uint16_t size; fread( size, __in);
    __v = vector<_Tp, _Alloc>(size);
    if( ::fread( __v.data(), sizeof(typename vector<_Tp, _Alloc>::value_type), __v.size(), __in ) != __v.size() ) throw rostlab::runtime_error( strerror(errno) );
}
template<>
inline void         fread<string>( string& __v, FILE* __in )
{
    uint16_t strlen; fread( strlen, __in);
    if( !strlen ){ __v = string(); return; }

    string::value_type buf[ sizeof( string::value_type ) * strlen ];
    size_t managed_to_read = ::fread( buf, sizeof( string::value_type ), strlen, __in );
    if( managed_to_read != strlen ) throw rostlab::runtime_error( strerror(errno) );
    __v = string( buf, managed_to_read );
}
// This must come last - uses the others.
template<typename _Tp>
inline _Tp          fread( FILE* __in ){ _Tp ret; fread( ret, __in ); return ret; }

} // namespace rostlab

#endif // ROSTLAB_STDIO
// vim:et:ts=4:ai:
